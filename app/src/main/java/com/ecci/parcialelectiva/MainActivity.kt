package com.ecci.parcialelectiva

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button: Button = findViewById(R.id.button)
        val errorTextView = findViewById<TextView>(R.id.errorTextView)
        val usuarioEditText = findViewById<EditText>(R.id.usuarioEditText)
        val contrasenaEditText = findViewById<EditText>(R.id.contrasenaEditText)


        //Creo un Listener, para cuando el botón sea pulsado
        button.setOnClickListener {
            //Voy a verificar el texto que el usuario puso en la caja de texto.
            val miNombre = usuarioEditText.text.toString()
            val miContrasena = contrasenaEditText.text.toString()

            //Si es vacio muestro un error.
            if (miNombre == "" || miContrasena == "") {
                errorTextView.text = "El usuario y la contraseña son obligatorios"
                return@setOnClickListener
            }
            //Si la contraseña es menor a 4 digitos muestra un error
            if (miContrasena.length <= 4) {
                errorTextView.text = "La contraseña es incorrecta"
                return@setOnClickListener
            }
            //Si el usuario es correcto se abre la actividad home
            if (miNombre.equals("juliane.vasqueza@ecci.edu.co")
                || miNombre.equals("sebastian.contrerasg@ecci.edu.co")
                || miNombre.equals("cmunozp@ecci.edu.co")){
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                return@setOnClickListener
            }
            //de lo contrario muestra un mensaje de error
           else  errorTextView.text = "El usuario es incorrecto"

        }

    }
}